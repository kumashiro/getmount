`getmount` is a simple tool for printing mount point, device or file system type
for current directory or given path.

#### Example

By default, `getmount` prints mount point path:

```sh
$ getmount /proc/sys/net/ipv4
/proc
```

It can also print device path:

```sh
$ getmount -d /var/spool/mail
/dev/sda4
```

...or file system type:

```sh
$ getmount -t /sys/class/scsi_disk/                                                                                                                                                                 
sysfs
```

#### Compilation and installation

Below is a list of additional software required or recommended for compilation:

  * `required` GNU autotools (aclocal, autoconf, automake)
  * `required` getopt (preferably with `getopt_long()`)
  * `required` gmake
  * `optional` GNU Gettext with tools (xgettext, msginit, msgmerge, msgfmt)

If *getopt* has no `getopt_long()` function, long options will not be available.
If *Gettext* is not present or disabled, error messages translations will not be
available.

To compile `getmount`, clone it and execute the following commands:

```sh
clone$ ./autoinit.sh
clone$ ./configure
clone$ gmake
...
clone# gmake install
```

You may wish to alter some options before compiling. This can be done by running
`./configure` with arguments. Try `./configure --help` to get a list of
switches.

When recompiling, it is advisable to start the whole process anew. Resetting the
project to its initial state can be done with command:

```sh
clone$ gmake squeky-clean
```
