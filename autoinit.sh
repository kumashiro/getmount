#!/bin/sh
#
# autoinit.sh  - Prepare build tools.
#
# This file is part of getmount project.
#
# getmount is  free software: you can  redistribute it and/or modify  it under
# the  terms of  the  GNU General  Public  License as  published  by the  Free
# Software Foundation,  either version 3 of  the License, or (at  your option)
# any later version.
#
# getmount is distributed in the hope that  it will be useful, but WITHOUT ANY
# WARRANTY; without  even the implied  warranty of MERCHANTABILITY  or FITNESS
# FOR  A PARTICULAR  PURPOSE.  See the  GNU General  Public  License for  more
# details.
#
# You should have received a copy of the GNU General Public License along with
# waitport. If not, see <http://www.gnu.org/licenses/>.
#
# Copyright (c) 2016-2017 Kaito Kumashiro <kumashiro.kaito@gmail.com>

ACLOCAL="`which aclocal`"
AUTOCONF="`which autoconf`"
AUTOMAKE="`which automake`"

if [ "${ACLOCAL}x" = "x" ]; then
    2>&1 echo "ERROR: Required \`aclocal' was not found"
    exit 1
fi

if [ "${AUTOCONF}x" = "x" ]; then
    2>&1 echo "ERROR: Required \`autoconf' not found"
    exit 1
fi

if [ "${AUTOMAKE}x" = "x" ]; then
    2>&1 echo "ERROR: Required \`automake' not found"
    exit 1
fi

"${ACLOCAL}" || exit 2
"${AUTOCONF}" || exit 3
"${AUTOMAKE}" --add-missing --copy 2> /dev/null

echo "You may now run ./configure (with or without desired options)"
exit 0

# vim: ft=sh ts=4 sts=4 sw=4 et
