/*
 * getmount is  free software: you can  redistribute it and/or modify  it under
 * the  terms of  the  GNU General  Public  License as  published  by the  Free
 * Software Foundation,  either version 3 of  the License, or (at  your option)
 * any later version.
 *
 * getmount is distributed in the hope that  it will be useful, but WITHOUT ANY
 * WARRANTY; without  even the implied  warranty of MERCHANTABILITY  or FITNESS
 * FOR  A PARTICULAR  PURPOSE.  See the  GNU General  Public  License for  more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * getmount. If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2017 Kaito Kumashiro <kumashiro.kaito@gmail.com>
 */

#ifndef MOUNT_H
#define MOUNT_H

#include <limits.h>

#include "config.h"

typedef enum {
    MOUNT_ERROR_OK,
    MOUNT_ERROR_MOUNTDB,
    MOUNT_ERROR_REALPATH,
    MOUNT_ERROR_STAT
} MountpointError;


typedef struct {
    char      device[PATH_MAX];
    char      fstype[GETMOUNT_FSTYPE_LENGTH];
    char      path[PATH_MAX];
} Mountpoint;


MountpointError  mountpoint_find(const char  *path, const char  *mountdbs, Mountpoint  **mount);
void  mountpoint_free(Mountpoint  *mount);

#endif /* MOUNT_H */
/* vim: set ft=c sw=4 sts=4 et: */
