/*
 * getmount is  free software: you can  redistribute it and/or modify  it under
 * the  terms of  the  GNU General  Public  License as  published  by the  Free
 * Software Foundation,  either version 3 of  the License, or (at  your option)
 * any later version.
 *
 * getmount is distributed in the hope that  it will be useful, but WITHOUT ANY
 * WARRANTY; without  even the implied  warranty of MERCHANTABILITY  or FITNESS
 * FOR  A PARTICULAR  PURPOSE.  See the  GNU General  Public  License for  more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * getmount. If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2017 Kaito Kumashiro <kumashiro.kaito@gmail.com>
 */

#ifndef _XOPEN_SOURCE
#define _XOPEN_SOURCE       700
#endif /* _XOPEN_SOURCE */

#include <errno.h>
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "config.h"
#include "gettext.h"
#include "help.h"
#include "mount.h"
#include "settings.h"

#ifndef PATH_MAX
#define PATH_MAX            4096
#endif /* PATH_MAX */


int  main(int  argc, char * const  argv[])
{
    int             err;
    Settings        *settings;
    Mountpoint      *mount;
    MountpointError merr;
    char            *result;


    setlocale(LC_ALL, "");
#ifdef GETTEXT
    bindtextdomain(PACKAGE_NAME, LOCALEDIR);
    textdomain(PACKAGE_NAME);
#endif /* GETTEXT */

    settings = settings_new();
    err = settings_parse_args(settings, argc, argv);
    if ( err != 0 || IS_SET(settings->flags, SETTINGS_FLAG_HELP) ) {
        help_print(err ? stderr : stdout);
        free(settings);
        return err;
    };

    if ( IS_SET(settings->flags, SETTINGS_FLAG_VERSION) ) {
        help_print_version(stdout);
        free(settings);
        return 0;
    };

    errno = 0;
    merr = mountpoint_find(settings->path, settings->mountdbs, &mount);
    switch ( merr ) {
        case MOUNT_ERROR_REALPATH:
            fprintf(stderr, _("Could not unroll path '%s': %s\n"), settings->path, strerror(errno));
            break;
        case MOUNT_ERROR_MOUNTDB:
            fprintf(stderr, _("No usable mount point database found. Current search list: %s\n"), settings->mountdbs);
            break;
        case MOUNT_ERROR_STAT:
            fprintf(stderr, _("Error getting filesystem parameters: %s\n"), strerror(errno));
            break;
        case MOUNT_ERROR_OK:
            if ( IS_SET(settings->flags, SETTINGS_FLAG_DEVICE) )
                result = mount->device;
            else if ( IS_SET(settings->flags, SETTINGS_FLAG_FSTYPE) )
                result = mount->fstype;
            else
                result = mount->path;

            fprintf(stdout, "%s\n", result);
            mountpoint_free(mount);
            break;
        default:
            fprintf(stderr, _("Unknown error occured: %d\n"), merr);
            break;
    };

    free(settings);
    return merr;
}

/* vim: set filetype=c expandtab tabstop=4 sts=4 shiftwidth=4: */
