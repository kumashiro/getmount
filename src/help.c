/*
 * getmount is  free software: you can  redistribute it and/or modify  it under
 * the  terms of  the  GNU General  Public  License as  published  by the  Free
 * Software Foundation,  either version 3 of  the License, or (at  your option)
 * any later version.
 *
 * getmount is distributed in the hope that  it will be useful, but WITHOUT ANY
 * WARRANTY; without  even the implied  warranty of MERCHANTABILITY  or FITNESS
 * FOR  A PARTICULAR  PURPOSE.  See the  GNU General  Public  License for  more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * getmount. If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2017 Kaito Kumashiro <kumashiro.kaito@gmail.com>
 */

#include <stdio.h>

#include "gettext.h"
#include "help.h"


void  help_print(FILE* stream)
{
    fprintf(stream, _(
        "Usage:\n\n"
        "  %s [ -h | -V ] [ -d | -t ] [ -m PATHS ] [ FILE ]\n\n"
        "  -d, --device                print device path\n"
        "  -h, --help                  print this help screen and exit\n"
        "  -m PATHS, --mountdbs PATHS  colon separated list of files to read mount points.\n"
        "                              Default: %s\n"
        "  -t, --type                  print file system type\n"
        "  -V, --version               print program name, version and exit\n"
        "  FILE                        existing directory or file to check.\n"
        "                              Default: current working directory\n\n"
        "NOTE: long options may not be available on some platforms\n"),
        PACKAGE_NAME, GETMOUNT_DEFAULT_MOUNTDBS
    );
}


void  help_print_version(FILE* stream)
{
    fprintf(stream, PACKAGE_NAME " " GETMOUNT_VERSION "\n"
            "Copyright (C) 2017 Kaito Kumashiro\n"
            "This program comes with ABSOLUTELY NO WARRANTY.\n"
            "This is free software, licensed under the terms of the GNU General Public License.\n"
            "For details, please see the LICENSE.md file that came with this program or point\n"
            "your browser to the web address https://www.gnu.org/licenses/gpl-3.0.en.html on the\n"
            "Internet, SkyNet, Galnet or whatever is available at your time.\n");
}

/* vim: set ft=c sw=4 sts=4 et: */
