/*
 * getmount is  free software: you can  redistribute it and/or modify  it under
 * the  terms of  the  GNU General  Public  License as  published  by the  Free
 * Software Foundation,  either version 3 of  the License, or (at  your option)
 * any later version.
 *
 * getmount is distributed in the hope that  it will be useful, but WITHOUT ANY
 * WARRANTY; without  even the implied  warranty of MERCHANTABILITY  or FITNESS
 * FOR  A PARTICULAR  PURPOSE.  See the  GNU General  Public  License for  more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * getmount. If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2017 Kaito Kumashiro <kumashiro.kaito@gmail.com>
 */

#ifndef _XOPEN_SOURCE
#define _XOPEN_SOURCE       700
#endif /* _XOPEN_SOURCE */

#include <errno.h>
#include <mntent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/vfs.h>
#include <unistd.h>

#include "config.h"
#include "gettext.h"
#include "mount.h"

#ifndef PATH_MAX
#define PATH_MAX            4096
#endif /* PATH_MAX */


static
char  *get_token(const char  *src, char  *dst, ssize_t  *dstlen)
{
    /*
     * By "token" we mean "a text element". In this case, text elements
     * are separated with colon.
     */
    char        *pos;
    size_t      token_len;


    if ( src == NULL || *src == '\0' )
        return NULL;

    pos = strchr(src, ':');
    token_len = pos == NULL ? strlen(src) : (size_t)(pos - src);
    if ( token_len > (size_t)*dstlen ) {
        *dstlen = -1;
        return NULL;
    };

    strncpy(dst, src, token_len);
    return pos == NULL ? pos : pos + 1;
}


static inline
size_t  common_length(const char  *base, const char  *path)
{
    size_t      len;


    for ( len = 0; base[len] != '\0' && path[len] != '\0'; len++ )
        if ( base[len] != path[len] )
            break;

    return base[len] == '\0' ? len : 0;
}


static inline
FILE  *open_mountdb(const char  *mountdbs)
{
    FILE        *fh;
    const char  *pos;
    char        path[PATH_MAX];
    ssize_t     len;


    fh = NULL;
    pos = mountdbs == NULL ? GETMOUNT_DEFAULT_MOUNTDBS : mountdbs;
    while ( pos != NULL && *pos != '\0' ) {
        len = sizeof(path);
        pos = get_token(pos, path, &len);
        if ( len <= 0 )
            continue;

        if ( (fh = setmntent(path, "r")) != NULL )
            break;
    };

    return fh;
}


static inline
Mountpoint  *mountpoint_new(void)
{
    Mountpoint  *mount;


    if ( (mount = (Mountpoint*)malloc(sizeof(Mountpoint))) == NULL )
        abort();

    memset(mount, 0, sizeof(Mountpoint));
    return mount;
}


static
MountpointError  get_from_fsid(unsigned long int  fsid, const char  *mountdbs, Mountpoint  **mount)
{
    FILE            *fh;
    struct mntent   *mnt;
    struct statfs   stfs;
    int             found;


    if ( (fh = open_mountdb(mountdbs)) == NULL ) {
        *mount = NULL;
        return MOUNT_ERROR_MOUNTDB;
    };

    found = 0;
    while ( (mnt = getmntent(fh)) != NULL ) {
        if ( statfs(mnt->mnt_dir, &stfs) != 0 ) {
            /*
             * Should we add some kind of debug option, with which we would
             * print warning here, instead of silently ignoring error and leaving
             * user oblivious about what happened? What if user made a mistake
             * when passing a list of mount database files? Should we punish him
             * for that? And why am I discussing this in the comment?
             */
            /* perror("statfs()"); */
            continue;
        };

        if ( memcmp(&fsid, &(stfs.f_fsid), sizeof(unsigned long int)) != 0 ) {
            continue;
        } else {
            found = 1;
            break;
        };
    };

    *mount = NULL;
    if ( found ) {
        *mount = mountpoint_new();
        strncpy((*mount)->device, mnt->mnt_fsname, PATH_MAX);
        strncpy((*mount)->fstype, mnt->mnt_type, GETMOUNT_FSTYPE_LENGTH);
        strncpy((*mount)->path, mnt->mnt_dir, PATH_MAX);
    };

    endmntent(fh);
    return MOUNT_ERROR_OK;
}


static
MountpointError  get_from_path(const char  *path, const char  *mountdbs, Mountpoint  **mount)
{
    size_t          len, longest;
    FILE            *fh;
    struct mntent   *mnt;


    if ( (fh = open_mountdb(mountdbs)) == NULL ) {
        *mount = NULL;
        return MOUNT_ERROR_MOUNTDB;
    };

    longest = 0;
    *mount = NULL;
    while ( (mnt = getmntent(fh)) != NULL ) {
        if ( (len = common_length(mnt->mnt_dir, path)) > longest ) {
            if ( *mount == NULL )
                *mount = mountpoint_new();
            strncpy((*mount)->device, mnt->mnt_fsname, PATH_MAX);
            strncpy((*mount)->fstype, mnt->mnt_type, GETMOUNT_FSTYPE_LENGTH);
            strncpy((*mount)->path, mnt->mnt_dir, PATH_MAX);
            longest = len;
        };
    };

    endmntent(fh);
    return MOUNT_ERROR_OK;
}


MountpointError  mountpoint_find(const char  *path, const char  *mountdbs, Mountpoint  **mount)
{
    char                rpath[PATH_MAX];
    struct statfs       stfs;
    unsigned long int   fsid;
    MountpointError     err;


    if ( realpath(path, rpath) == NULL )
        return MOUNT_ERROR_REALPATH;
    else
        rpath[sizeof(rpath) - 1] = '\0';

    if ( statfs(rpath, &stfs) != 0 )
        return MOUNT_ERROR_STAT;

    err = MOUNT_ERROR_OK;
    memcpy(&fsid, &(stfs.f_fsid), sizeof(unsigned long int));
    if ( fsid != 0 )
        err = get_from_fsid(fsid, mountdbs, mount);

    if ( err == MOUNT_ERROR_MOUNTDB )
        return err;

    if ( fsid == 0 || mount == NULL )
        /*
         * fsid != 0 and mount == NULL at this point should not happen,
         * but just in case, if it does, we fall back to crude path comparison.
         */
        err = get_from_path(rpath, mountdbs, mount);

    return err;
}


void  mountpoint_free(Mountpoint  *mount)
{
    if ( mount != NULL )
        free(mount);
}

/* vim: set ft=c sw=4 sts=4 et: */
