/*
 * getmount is  free software: you can  redistribute it and/or modify  it under
 * the  terms of  the  GNU General  Public  License as  published  by the  Free
 * Software Foundation,  either version 3 of  the License, or (at  your option)
 * any later version.
 *
 * getmount is distributed in the hope that  it will be useful, but WITHOUT ANY
 * WARRANTY; without  even the implied  warranty of MERCHANTABILITY  or FITNESS
 * FOR  A PARTICULAR  PURPOSE.  See the  GNU General  Public  License for  more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * getmount. If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2017 Kaito Kumashiro <kumashiro.kaito@gmail.com>
 */

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>

#include "gettext.h"
#include "settings.h"

#define ARGSPEC     "dhm:tV"

#ifdef HAVE_GETOPT_LONG
static struct option    long_options[] = {
    {"device",            no_argument, NULL, 'd'},
    {"help",              no_argument, NULL, 'h'},
    {"mountdbs",    required_argument, NULL, 'm'},
    {"type",        required_argument, NULL, 't'},
    {"version",           no_argument, NULL, 'V'},
    {NULL,                          0, NULL,   0}
};
#endif /* HAVE_GETOPT_LONG */


Settings  *settings_new(void)
{
    Settings    *settings;
    char        *env;


    if ( (settings = (Settings*)malloc(sizeof(Settings))) == NULL )
        abort();

    settings->path = NULL;
    settings->flags = SETTINGS_FLAG_NONE;

    if ( (env = getenv("GETMOUNT_MOUNT_DATABASES")) != NULL )
        settings->mountdbs = env;
    else
        settings->mountdbs = GETMOUNT_DEFAULT_MOUNTDBS;

    return settings;
}


int  settings_parse_args(Settings  *settings, int  argc, char * const  *argv)
{
    int         arg;


    do {
#ifdef HAVE_GETOPT_LONG
        arg = getopt_long(argc, argv, ARGSPEC, long_options, NULL);
#else
        arg = getopt(argc, argv, ARGSPEC);
#endif /* HAVE_GETOPT_LONG */
        switch ( arg ) {
            case 'd':
                settings->flags |= SETTINGS_FLAG_DEVICE;
                break;
            case 'h':
                settings->flags |= SETTINGS_FLAG_HELP;
                return 0;
            case 'm':
                settings->mountdbs = optarg;
                break;
            case 't':
                settings->flags |= SETTINGS_FLAG_FSTYPE;
                break;
            case 'V':
                settings->flags |= SETTINGS_FLAG_VERSION;
                return 0;
            case '?':
                return 1;
            default:
                break;
        };
    } while ( arg != -1 );

    if ( IS_SET(settings->flags, SETTINGS_FLAG_DEVICE) && IS_SET(settings->flags, SETTINGS_FLAG_FSTYPE) ) {
        fprintf(stderr, _("Options -d and -t are mutually exclusive\n"));
        return 1;
    };

    if ( optind > argc + 1 ) {
        fprintf(stderr, _("Too many positional arguments\n"));
        return 1;
    } else if ( optind == argc ) {
        settings->path = ".";
    } else {
        settings->path = argv[optind];
    };

    return 0;
}

/* vim: set ft=c sw=4 sts=4 et: */
