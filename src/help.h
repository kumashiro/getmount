/*
 * getmount is  free software: you can  redistribute it and/or modify  it under
 * the  terms of  the  GNU General  Public  License as  published  by the  Free
 * Software Foundation,  either version 3 of  the License, or (at  your option)
 * any later version.
 *
 * getmount is distributed in the hope that  it will be useful, but WITHOUT ANY
 * WARRANTY; without  even the implied  warranty of MERCHANTABILITY  or FITNESS
 * FOR  A PARTICULAR  PURPOSE.  See the  GNU General  Public  License for  more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * getmount. If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2017 Kaito Kumashiro <kumashiro.kaito@gmail.com>
 */

#ifndef HELP_H
#define HELP_H

void  help_print(FILE  *stream);
void  help_print_version(FILE  *stream);

#endif /* HELP_H */
/* vim: set ft=c sw=4 sts=4 et: */
